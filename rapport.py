#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
from numpy.lib.stride_tricks import broadcast_arrays
from pandas import DataFrame,read_csv
import geopandas as gpd
import contextily as ctx
import matplotlib.pyplot as plt
# import pycen
from pycen.zh import zh
from os import remove
zh = zh()

from fpdf import FPDF, image_parsing


img1  = 'img1.png'
img2  = 'img2.png'
img3  = 'img3.png'
# fout = '/home/colas/Documents/9_PROJETS/3_PGZH/rapport_test.pdf'
# fout = path0 + 'RAPPORT_ZH/%s.pdf' % code_site
path0 ='/home/colas/Documents/9_PROJETS/1_ZH/'
path_logo = path0 + 'FICHES_ZH/LOGOS/'
path_imgConnex = path0 + 'FICHES_ZH/CONNEX_IMG/'
path_wave = path0 + 'FICHES_ZH/Mise en page/'
wave = path_wave + 'Vague_CEN38-01.png'
# main_title_color = (0,67,147)     # code rvb bleu  charte-CEN
# title_color = (0,67,147)          # code rvb bleu  charte-CEN
main_title_color = (0,0,0)
title_color = (0,0,0)


lst_fil = '../3_PGZH/PGSZH_liste_ZH.csv'
# lst_fil = 'SIMBY_zonage_Belledonne.txt'
lst = read_csv(path0 + lst_fil,sep=";")
# code_site = '38CG0037'
lst_codesite = [*lst.site_code]
# lst_codesite = [code_site]

lst_logo = {
    'cen': {'ratio':2.61,'logo':path_logo+'CEN38-LOGO-140317_detoure.png',},        # ratio L/H
    'partenaire':{
        'dep_is':{'ratio':2.33,'logo':path_logo+'Isere_departement.PNG'},           # ratio L/H
        'ag_eau':{'ratio':1.1,'logo':path_logo+'LOGO-AGENCE-RVB.png'},              # ratio L/H
        'region':{'ratio':4.32,'logo':path_logo+'LOGO_REGION_RVB-BLEU-GRIS.png'},   # ratio L/H
    }}
connex_img = {
    'Traversée' : 'MWD1.JPG',
    'Entrée et sortie' : 'MWD2.JPG',
    'Entrée' : 'MWD3.JPG',
    'Sortie' : 'MWD4.JPG',
    'Passe à coté' : 'MWD5.JPG',
    'Aucune connexion' : 'MWD6.JPG',
    'Inconnu' : None
}

dictio = {
    ' LR' : ' Liste Rouge',
    ' PR' : ' protection régionale',
    ' PN' : ' protection nationale',
    ' DH' : ' Directive Habitats',
    ' REDI' : " réseau écologique du département de l'Isère",
}

replace_spetialChar = {
    '…':'...',
    'œ': 'oe'
}

class PDF(FPDF):
    def __init__(self,code=''):
        super().__init__()
        self.col = 0  # Current column
        self.y0 = 0   # Ordinate of column start
        self.r_margin = 10
        self.l_margin = self.r_margin
        self.set_auto_page_break(auto=True, margin=self.b_margin+5)
        self.code = code
        # self.t_margin = 20
        # self.b_margin = self.t_margin

    def header(self):
        # self.ln(5)
        # Logo Conservatoire
        H = 11
        X = 10
        Y = self.y
        r = lst_logo['cen']['ratio']
        logo = lst_logo['cen']['logo']
        # self.set_draw_color(0, 80, 180)
        # self.set_fill_color(230, 230, 0)
        # self.set_text_color(220, 50, 50)
        self.set_draw_color(255, 255, 255)
        self.set_fill_color(255, 255, 255)
        self.set_text_color(*main_title_color)
        self.set_line_width(1)
        # self.y = self.y + 5
        self.set_font("Helvetica", "", 8)
        width = self.get_string_width(self.code) + 6
        self.set_x((210 - width))
        self.cell(w=5, h=9, txt=self.code, border=1, ln=1, align="C", fill=True)
        self.y = self.y - 5

        self.set_font("Helvetica", "B", 18)
        width = self.get_string_width(self.title) + 6
        self.set_x((210 - width) / 2)
        if len(self.title) < 30:
            self.cell(w=width, h=9, txt=self.title, border=1, ln=1, align="C", fill=True)
            self.ln(10)
        else:
            width = 100
            self.set_x((210 - width) / 2)
            self.set_font("Helvetica", "B", 18)
            self.multi_cell(w=width, h=9, txt=self.title, border=1, ln=1, align="C", fill=True)
            # self.ln()
        self.image(logo,x=X ,y=Y,w=H*r,h=H)
        # self.ln(10)
        # Saving ordinate position:
        self.y0 = self.get_y()
        

    def footer(self):
        self.set_y(-15)
        self.set_x(10)
        size = 182
        X = self.x
        Y = self.y
        H = 8
        len_logo = len(lst_logo['partenaire'])
        l_logo = len_logo - 1
        for i,j in enumerate(lst_logo['partenaire']):
            r = lst_logo['partenaire'][j]['ratio']
            logo = lst_logo['partenaire'][j]['logo']
            if i == 0:
                self.image(logo,x=X ,y=Y-5,w=H*r,h=H)
            elif i == l_logo:
                self.image(logo,x=X+size-(H*r) ,y=Y-5,w=H*r,h=H)
            else:
                self.image(logo,x=(X+(size*(i/l_logo))) ,y=Y-5,w=H*r,h=H)
        self.set_font("Helvetica", "I", 8)
        self.set_text_color(128)
        self.ln(3)
        self.cell(0, 10, f"Page {self.page_no()}", 0, 0, "C")

    def set_col(self, col):
        # Set column position:
        self.col = col
        x = 10 + col * 65
        self.set_left_margin(x)
        self.set_x(x)

    # @property
    # def accept_page_break(self):
    #     if self.col < 2:
    #         # Go to next column:
    #         self.set_col(self.col + 1)
    #         # Set ordinate to top:
    #         self.set_y(self.y0)
    #         # Stay on the same page:
    #         return False
    #     # Go back to first column:
    #     self.set_col(0)
    #     # Trigger a page break:
    #     return True

    def chapter_title(self, num, label):
        self.set_font("Helvetica", "", 12)
        # self.set_fill_color(0,67,147)
        self.set_fill_color(200, 220, 255)
        self.cell(0, 6, f"Chapter {num} : {label}", 0, 1, "L", True)
        self.ln(4)
        # Saving ordinate position:
        self.y0 = self.get_y()

    def chapter_body(self, txt):
        # Reading text file:
        # with open(name, "rb") as fh:
        #     txt = fh.read().decode("latin-1")
        # Setting font: Helvetica 12
        self.set_font("Helvetica", size=12)
        # Printing text in a 6cm width column:
        self.multi_cell(90, 5, txt)
        self.ln()
        # Final mention in italics:
        # self.set_font(style="I")
        # self.cell(0, 5, "(end of excerpt)")
        # Start back at first column:
        self.set_col(0)

    def print_chapter(self, num, title, txt):
        # self.add_page()
        # self.chapter_title(num, title)
        self.chapter_body(txt)


def __formatSubtitle1(pdf,size=12):
    pdf.set_font(family="Helvetica",style="B", size=size) # font and textsize
    pdf.set_text_color(*title_color)

def __formatSubtitle2(pdf,style="B",size=11):
    pdf.set_font(family="Helvetica",style=style, size=size) # font and textsize
    pdf.set_text_color(*title_color)

def __formatText(pdf,size=10):
    pdf.set_font(family="Helvetica", size=size) # font and textsize
    pdf.set_text_color(0)

def __separateLine(pdf):
    pdf.set_fill_color(200, 220, 255)
    pdf.ln(2)
    # pdf.cell(0, 3, "", 0, 1, "L", True)
    pdf.ln(5)

def __fromatValue(df,col):
    df.loc[df[col]!='',col] = df.loc[df[col]!='',col].str[0].str.upper() + df.loc[df[col]!='',col].str[1:]
    df[col].replace(
        replace_spetialChar,
        regex=True,inplace=True)
    return df


def Titre(pdf,titre):
    pdf.set_title(titre) # Titre du fichier
    pdf.set_font("Helvetica","B", size=24) # font and textsize
    pdf.set_text_color(*title_color)
    pdf.cell(200, 10, txt=titre, ln=1, align="C") # Titre de la Pge
    pdf.set_text_color(0)
    pdf.ln(10)

def FisrtLign(pdf,id_site,site_area,commune):
    
    # pdf.ln()
    top = pdf.y # Save top coordinate
    offset = pdf.x + 40 # Calculate x position of next cell
    # ID_SITE
    pdf.set_font("Helvetica", size=12) # font and textsize
    # pdf.multi_cell(50, 10, txt='**'+id_site+'**', align="C",
    # markdown=True)
    # COMMUNE
    # pdf.y = top # Reset y coordinate
    # pdf.x = offset # Move to computed offset
    # pdf.set_col(1)
    txt_a = "**Commune(s) :** %s " % (commune)
    pdf.multi_cell(140, 5, txt=txt_a,markdown=True, align="L")#,border=1)
    # SUPERFICIE
    pdf.set_col(2)
    pdf.y = top # Reset y coordinate
    # pdf.x = offset # Move to computed offset
    txt_a = " **Superficie :** %s ha" % (site_area)
    pdf.multi_cell(60, 5, txt=txt_a,markdown=True, align="R" )#,border=1)
    pdf.set_col(0)
    pdf.ln()
    pdf.image(wave,x=pdf.x-5,y=pdf.y,w=200,h=15.6 )
                # self.image(logo,x=(X+(size*(i/l_logo))) ,y=Y-5,w=H*r,h=H)
    pdf.ln(10)

def auteur_crea(pdf,auteur,date,size=11):
    __formatSubtitle1(pdf,size=size+1)
    pdf.cell(100, 7, txt='Auteur et date du relevé', ln=1, align="L")
    __formatText(pdf,size=size)
    pdf.cell(100, 4, txt=auteur, ln=1, align="L")
    pdf.cell(100, 4, txt=date, ln=1, align="L")
    # pdf.ln(3)

def auteur_maj(pdf,auteur,date,size=11):
    __formatSubtitle1(pdf,size=size+1)
    pdf.cell(100, 7, txt='Dernière mise à jour', ln=1, align="L")
    __formatText(pdf,size=size)
    pdf.cell(100, 4, txt=auteur, ln=1, align="L")
    pdf.cell(100, 4, txt=date, ln=1, align="L")
    # pdf.ln(3)

def para(pdf,subtitle,text,column=None,width=200,heigth=5,border=0,size=11,ln=5):
    __formatSubtitle1(pdf,size+1)
    pdf.cell(width, 8, txt=subtitle, ln=1, align="J",border=border)
    __formatText(pdf,size)

    if isinstance(text,str):
        pdf.multi_cell(width, heigth, txt=text, ln=2, align="J",border=border)
    elif isinstance(text,DataFrame):
        if not column:
            raise "No column specify"
        else:
            if text[column].empty:
                print(" column %s empty ..." % column)
            else: 
                for i,j in text.iterrows():
                    pdf.multi_cell(width, heigth, txt=text[column][i], ln=2, align="L",border=border)

    pdf.ln(ln)

def Infos_zh(pdf,info,delm,last_maj=None):
    char_size = 8
    aut_crea  = info.auteur.values[0]
    date_crea = info.date_deb.values[0]
    aut_maj   = geom.auteur_last_maj.values[0]
    date_maj  = geom.date_last_maj.values[0]
    if last_maj : 
        if date_maj < last_maj['dat']:
            date_maj = last_maj['dat']
            aut_maj  = last_maj['usr']
    date_maj   = date_maj.isoformat()
    date_crea  = date_crea.isoformat()
    typ_sdage  = info.typo_sdage.values[0]
    crit_delim = delm[delm.type == 'crit_delim'].sort_values('nom_crit')

    auteur_crea(pdf,aut_crea,date_crea,size=char_size)
    auteur_maj(pdf,aut_maj,date_maj,size=char_size)
    sst1 = 'Typologie SDAGE'
    para(pdf,sst1,typ_sdage, width=95,size=char_size,ln=0)
    set_y = pdf.y
    sst1 = 'Critères de délimitation de la zone humide'
    para(pdf,sst1,crit_delim,'nom_crit', width=95,size=char_size)
    return set_y

def Table_fcmt_zh(pdf,eau_in,eau_out,typ_con):
    nrow = max([eau_in.shape[0]+2,eau_out.shape[0]+2,3])
    with pdf.table(width=170, col_widths=(30, 30, 50, 30, 30),
        text_align=("LEFT", "LEFT", "CENTER", "LEFT", "LEFT"), borders_layout="MINIMAL") as table:
        for irow in range(nrow):
            row = table.row()
            print(irow)
        tab = table
        sst1 = 'Hydrologie de la zone humide'
        sst2 = "--Alimentation en eau--"

def Fcmt_zh(pdf,eau_in,eau_out,typ_con):
    # Fonctionnement de la zone humide
    # Partie 1 : Entrée
    sst1 = 'Hydrologie de la zone humide'
    sst2 = "--Alimentation en eau--"
    __formatSubtitle1(pdf)
    pdf.cell(100, 8, txt=sst1, ln=1, align="L")
    topsst = pdf.y
    offset = pdf.x
    __formatSubtitle2(pdf)
    pdf.cell(100, 8, txt=sst2, ln=1, align="L",markdown=True)
    toptxt = pdf.y
    __formatSubtitle2(pdf,style="I",size=10)
    pdf.multi_cell(50, 5, txt="type", ln=2, align="L");pdf.ln(1)
    __formatText(pdf)
    for i,j in eau_in.iterrows():
        pdf.multi_cell(50, 5, txt=eau_in.regime_hydri[i], ln=2, align="L")
    pdf.set_col(0.5)    # set column
    pdf.set_y(toptxt)   # Reset y coordinate
    __formatSubtitle2(pdf,style="I",size=10)
    pdf.multi_cell(50, 5, txt="permanence", ln=2, align="L");pdf.ln(1)
    __formatText(pdf)
    for i,j in eau_in.iterrows():
        perm_in = eau_in.permanence[i] if eau_in.permanence[i] else 'Non renseigné'
        pdf.multi_cell(50, 5, txt=perm_in, ln=2, align="L")
    downtxt = pdf.y
    pdf.set_col(0)
    pdf.line(10, 30, 110, 30)
    # image connexion
    # typ_con = subc.connexion.dropna().unique()[0]
    if typ_con and typ_con != 'Inconnu':
        pdf.set_y(topsst-1)   # Reset y coordinate
        pdf.set_col(1.1)      # set column
        __formatSubtitle2(pdf,style="I",size=10)
        pdf.x = 0
        pdf.cell(200, 8, txt='Circulation des eaux', ln=1, align="C",markdown=True)
        __formatText(pdf)
        i_con = path_imgConnex + connex_img[typ_con]
        i = image_parsing.get_img_info(i_con)
        size  = 25
        r = i['w'] / i['h']
        W = size
        H = size / r
        X = 200/2 - W/2
        pdf.image(i_con, x=X, y=pdf.y, w=W, h=H)
        pdf.set_y(pdf.y+H)
        pdf.x = 0
        pdf.cell(200, 8, txt=typ_con, ln=1, align="C",markdown=True)
        if pdf.y + 10 > downtxt:
            downtxt = pdf.y + 10
        # if endx < pdf.x:
        #     endx = pdf.x

    # Partie 2 : Sortie
    pdf.set_y(topsst)   # Reset y coordinate
    pdf.set_col(2)      # set column
    sst2 = "--Evacuation de l'eau--"
    __formatSubtitle2(pdf)
    pdf.cell(100, 8, txt=sst2, ln=1, align="L",markdown=True)
    toptxt = pdf.y
    __formatSubtitle2(pdf,style="I",size=10)
    pdf.multi_cell(50, 5, txt="type", ln=2, align="L");pdf.ln(1)
    __formatText(pdf)
    for i,j in eau_out.iterrows():
        pdf.multi_cell(50, 5, txt=eau_out.regime_hydri[i], ln=2, align="L")
    pdf.set_col(2.5)    # set column
    pdf.set_y(toptxt)   # Reset y coordinate
    __formatSubtitle2(pdf,style="I",size=10)
    pdf.multi_cell(50, 5, txt="permanence", ln=2, align="L");pdf.ln(1)
    __formatText(pdf)
    for i,j in eau_out.iterrows():
        perm_out = eau_out.permanence[i] if eau_out.permanence[i] else 'Non renseigné'
        pdf.multi_cell(50, 5, txt=perm_out, ln=2, align="L")

    if pdf.y > downtxt:
        downtxt = pdf.y + 10
    pdf.set_y(downtxt)   # Reset y coordinate
    pdf.set_col(0)    # set column
    # pdf.ln(5)

def Habs_zh(pdf,habitat,img,offset):
    # top = pdf.y + 5            # Save top coordinate
    sst1 = 'Habitats'
    sst2 = "code Corine Biotope"
    __formatSubtitle1(pdf)
    topY = pdf.y
    pdf.cell(40, 8, txt=sst1, ln=1, align="L")
    __formatSubtitle2(pdf,style="I",size=10)
    pdf.set_col(1.25)
    pdf.set_y(topY)
    pdf.cell(20, 8, txt=sst2, ln=1, align="C") #,border=1)
    pdf.set_col(0)
    I = image_parsing.get_img_info(img)
    W = I['w']/6
    H = I['h']/6
    pdf.image(img,x=offset,y=topY,w=W,h=H)
    # top = pdf.y             # Save top coordinate
    __formatText(pdf)
    if not habitat.empty:
        if habitat.lb_hab_fr.unique()[0] != '' :
            endY = topY
            habitat.sort_values('lb_hab_fr',inplace=True)
            for i,j in habitat.iterrows():
                txt = "%s" % (habitat.lb_hab_fr[i])
                topY = pdf.y
                pdf.multi_cell(80, 5, txt=txt, ln=2, align="J") #,border=1)
                endY = pdf.y
            
                moinsY = topY-endY
                if '-' != str(moinsY)[0]:
                    moinsY = -5
                # print(topY-endY)
                # pdf.col = 1.25
                pdf.set_col(1.3)
                pdf.set_y(pdf.y+moinsY)
                pdf.multi_cell(15, 5, txt=habitat.code_cb[i], ln=2, align="L") #,border=1)
                pdf.set_col(0)
                pdf.set_y(endY)
        else:
            print(" column %s empty ..." % 'lb_hab_fr')
            pdf.cell(100, 5, txt='Non documenté', ln=2, align="L")
            pdf.ln(15)
    else:
        print(" column %s empty ..." % 'lb_hab_fr')
        pdf.cell(100, 5, txt='Non documenté', ln=2, align="L")
        pdf.ln(15)
    pdf.ln()

def UsgProcess_zh(pdf, usgproc):
    sst1 = 'Usages ou processus naturels'
    # usgproc = __fromatValue(usgproc,'remarques')
    __formatSubtitle1(pdf)
    pdf.cell(100, 8, txt=sst1, ln=1, align="L")
    endcell = pdf.y             # Save top coordinate
    __formatText(pdf)
    for i,j in usgproc.iterrows():
        impact = usgproc.impact[i]
        descp  = usgproc.desc_param_pos[i]
        if impact.startswith('Autre'):
            impact = usgproc.remarques[i]

        pdf.set_y(endcell)
        top = pdf.y             # Save top coordinate
        txt = "%s" % (impact)
        pdf.multi_cell(70, 5, txt=txt, ln=2, align="L")
        endcell = pdf.y             # Save top coordinate
        
        pdf.set_y(top)
        pdf.set_col(1.1)
        txt = "%s" % (descp)
        pdf.multi_cell(120, 5, txt=txt, ln=2, align="L")
        pdf.set_col(0)
        if endcell < pdf.y:
            endcell = pdf.y             # Save top coordinate
    # pdf.set_y(top)
    # pdf.set_col(1)
    # for i,j in usgproc.iterrows():
    #     txt = "Localisation : %s" % (usgproc.desc_param_pos[i])
    #     pdf.multi_cell(150, 5, txt=txt, ln=2, align="L")
    # pdf.set_col(0)      # reset column
    pdf.ln(5)

def Fcts_zh(pdf,type_fct,sst):

    save_y = pdf.y
    __formatSubtitle1(pdf)
    pdf.cell(100, 8, txt=sst, ln=1, align="L")
    __formatText(pdf)
    # sst1 = 'Fonctions hydrologiques :'
    # tmp = fcts[fcts.type==type_fct]
    if type_fct.empty:
        txt = "Non documenté"
        pdf.cell(100, 8, txt=txt, ln=1, align="L")
    else:
        cell = 190
        is_intpatri = type_fct.type.unique()[0]=='int_patri'
        # if type_fct.type.unique()[0]=='int_patri': 
        #     make_plot = type_fct.nom_fct.isin(lst_sp).any()
        #     if make_plot :
        #         cell = 100
        #     else : cell = 190
        # else : cell = 190
        if is_intpatri:

            from pandas import Index
            lst_sp = zh._get_param(param_table='param_fct_eco_socio_patri',type_table='type_param_fct')
            lst_sp.nom = lst_sp.nom.str[0].str.upper() + lst_sp.nom.str[1:]
            lst_sp = lst_sp[lst_sp.type=='int_patri'].nom
            lst_sp = Index(lst_sp).drop(
                ['Aucun intérêt patrimonial','Habitats','Non documenté',])
                
            make_plot = type_fct.nom_fct.isin(lst_sp).any()
            if make_plot:
                colormap = {
                    'Invertébrés':'#A64D79',
                    'Algues':'#E06666',
                    'Mammifères':'#ffd966',
                    'Phanérogames':'#f6b26b',
                    'Lichens':'#CE7E00',
                    'Bryophytes':'#a36500',
                    'Champignons':'#523200',
                    'Floristiques':'#b1da65',
                    'Amphibiens':'#72A400',
                    'Reptiles':'#38761d',
                    'Faunistiques':'#45818e',
                    'Oiseaux':'#3D85C6',
                    'Poissons':'#16537e',
                    'Insectes':'#6a329f',
                    'Ptéridophytes':'#8e7cc3',
                }
                cell = 100
                plot = type_fct[type_fct.nom_fct.isin(lst_sp)].copy()
                plot.set_index('nom_fct', inplace=True)
                plot.sort_index(inplace=True)
                plot.quantite = plot.quantite.astype(int)
                ax = plot.plot.pie(
                    y='quantite',
                    title="Proportion d'espèces patrimoniales sur la zone.",
                    figsize=(5, 5),
                    legend=False,
                    ylabel='',
                    colors = [colormap[i] for i in plot.index],
                    fontsize=11,
                    # autopct='%1.1f%%', # pourcentage sur 1 chiffre apres la virgule de chaque portion 
                )
                ax.title.set_fontweight('bold')
                # ax.
                plt.savefig(
                    img3,
                    transparent=True,
                )
                pdf.image(img3,x=120,y=save_y,w=70,h=70)
                remove(img3)
        for i,j in type_fct.iterrows():
            if type_fct.description[i]:
                txt = "%s (%s)" % (type_fct.nom_fct[i],type_fct.description[i])
            else:
                txt = "%s" % (type_fct.nom_fct[i])
            pdf.multi_cell(cell, 5, txt=txt, ln=2, align="J")
        
        if is_intpatri and make_plot:
            pdf.set_y(save_y+55)

    pdf.ln()


def _redimensionMap(map):
    R = 1.35
    xmin, xmax, ymin, ymax = map.axis()
    X = (xmax - xmin).round(2)
    Y = (ymax - ymin).round(2)
    r = (X / Y).round(2)
    z = R / r
    if z > 1:
        xmin, xmax = xmin-X/2*z, xmax+X/2*z
    else:
        ymin, ymax = ymin-Y/2*z, ymax+Y/2*z
    map.axis(
        ( xmin, xmax, ymin, ymax,)
    )
    return map

    
def create_maps(geom, map1, map2, zoom='auto'):
    geom = geom.copy()
    maps1 = geom.to_crs(epsg=3857).copy()
    maps2 = geom.to_crs(epsg=3857).copy()
    create_map = False
    while create_map==False :
        zooom = ['auto',16,15,14,13,12,11,10]
        for z in zooom:
            ax = maps1.plot(alpha=0.5, edgecolor='k')
            # xmin, xmax, ymin, ymax = tuple(x for x in ax.axis())
            # print(ax.axis())
            ax = _redimensionMap(ax)
            xmin, xmax, ymin, ymax = ax.axis()
            add = (xmax-xmin)/5
            # add = (ymax-ymin)/5
            xmin, xmax, ymin, ymax = xmin-add, xmax+add, ymin-add, ymax+add
            ax.axis(
                (xmin, xmax, ymin, ymax)
            )
            try:
                ctx.add_basemap(
                    ax, 
                    attribution_size=5,
                    reset_extent=False,
                    source=map1,
                    zoom=z
                    # zoom=14
                )
                ax.set_axis_off()
                plt.savefig(
                    img1,
                    transparent=True,
                )
                create_map = True
                print("Map1 zoom %s OK !" % z)
            except:
                print("NO zoom %s" % z)
            if create_map :
                break

    print('Init map2')
    ax2 = maps2.plot(
        alpha=0.5, 
        edgecolor='k')
    
    ax2 = _redimensionMap(ax2)

    ctx.add_basemap(
        ax2, 
        attribution_size=5,
        reset_extent=True,
        source=map2,
        zoom=zoom
    )
    ax2.set_axis_off()
    plt.savefig(
        img2,
        transparent=True,
    )
    plt.close('all')
    print('END map !')


def round_up(n, decimals=0):
    import math
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier


def create_pdf(df,code_site,fileprio=None,rvb_priorite=None,rvb_objectif=None):
    import datetime

    def render_table(fileprio,code_site,rvb_priorite,rvb_objectif):
        # Source : https://pyfpdf.github.io/fpdf2/Tables.html
        def render_table_header(TABLE_COL,color=(22,65,148),textcolor=(0,0,0)):
        # def render_table_header(TABLE_COL,color=(73,95,169)):
            c = color
            t = textcolor
            pdf.set_font(style="B")  # enabling bold text
            pdf.set_text_color(t[0],t[1],t[2])
            pdf.set_fill_color(c[0],c[1],c[2])
            for col_name in TABLE_COL:
                pdf.cell(col_width, line_height, col_name, border=1,fill=True,align="C")
            pdf.ln(line_height)
            pdf.set_font(style="")  # disabling bold text
        
        TITLE = "Priorisation - Plan de gestion stratégique des zones humides - Sud Grésivaudan - 2023"
        lst_columns = [
            ['Fonction biologique','Fonction hydrologique','Fonction physico-chimique','Bilan des fonctions'],
            ['Pressions directes','Pressions indirectes','Bilan des pressions'],
            ['Objectif prioritaire', "Fiche - actions sectorisées"]
        ]

        line_height = pdf.font_size * 2
        col_width = pdf.epw
        render_table_header((TITLE,),color=(182,212,132))
        for c,columns in enumerate(lst_columns):
            TABLE = (gpd.pd.read_excel(fileprio,sheet_name="Enjeux")
                .rename(columns=dict_cols_readEnjeux)
                .dropna(axis=1,how='all'))
            TABLE.set_index('id_site',inplace=True)
            TABLE = TABLE[columns]

            # TABLE_COL_NAMES = ("First name", "Last name", "Age", "City")
            # TABLE_DATA = (
            #     ("Jules", "Smith", "34", "San Juan"),
            #     ("Mary", "Ramos", "45", "Orlando"),
            #     ("Carlson", "Banks", "19", "Los Angeles"),
            #     ("Lucas", "Cimon", "31", "Angers"),
            # )
            TABLE_COL_NAMES = tuple(TABLE.rename(columns=dcols_pdfEnjeux).columns)
            TABLE_DATA  = (tuple(TABLE[TABLE.index==code_site].values[0]),)
            col_width   = pdf.epw / len(TABLE_COL_NAMES)  # distribute content evenly
            # if c == 2:
            #     col_width = col_width*2

            render_table_header(TABLE_COL_NAMES,textcolor=(173,173,173))
            # render_table_header(TABLE_COL_NAMES,color=(91,91,91))
            for row in TABLE_DATA:
                if pdf.will_page_break(line_height):
                    render_table_header()
                
                if gpd.pd.isna(row[1]):
                    row = tuple((row[0],'Non concerné'))

                roundup = round_up(max([len(x) for x in row])/(116/len(row)))
                if roundup > 1:
                    line_height = line_height*roundup
                else : line_height = pdf.font_size * 2

                for datum in row:
                    if datum in rvb_priorite.id.tolist():
                        x = tuple(rvb_priorite.loc[rvb_priorite.id==datum,['r','v','b']].values[0])
                        pdf.set_fill_color(x[0],x[1],x[2])
                    else:pdf.set_fill_color(255,255,255)

                    if datum in rvb_objectif.id.tolist():
                        y = tuple(rvb_objectif.loc[rvb_objectif.id==datum,['r','v','b']].values[0])
                        pdf.set_font(style="B")
                        pdf.set_text_color(y[0],y[1],y[2])
                    else:pdf.set_text_color(0)
                    
                    cell = False if round_up(len(datum)/(116/len(row))) > 1 else True

                    if cell:
                        pdf.cell(col_width, line_height, datum, border=1,align="C",fill=True)
                    else:
                        pdf.multi_cell(col_width, pdf.font_size * 2, datum, border=1,align="C",fill=True)
                    
                pdf.ln(pdf.font_size * 2)



    df = df.copy()
    info = df['infos'].fillna('')
    info = info[info.id == code_site]
    habs = df['corine_biotope'].fillna('')
    habs = habs[habs.id_site == code_site]
    delm = df['delimitation'].fillna('')
    delm = delm[delm.id_site == code_site]
    delm = __fromatValue(delm,'nom_crit')
    desc = df['description'].fillna('')
    desc = desc[desc.id_site == code_site]
    desc = __fromatValue(desc,'remarques')
    desc = __fromatValue(desc,'impact')
    o_in = df['fonctionnement']['entree_eau'].fillna('')
    o_in = o_in[o_in.id_site == code_site]
    o_ou = df['fonctionnement']['sortie_eau'].fillna('')
    o_ou = o_ou[o_ou.id_site == code_site]
    subc = df['fonctionnement']['sub_connex'].fillna('')
    subc = subc[subc.id_site == code_site]
    subc.connexion.replace({'':None},inplace=True)
    if 'date_sub' in subc.columns:
        subc.date_sub.replace({'':None},inplace=True)
    subc.date_conne.replace({'':None},inplace=True)
    fcts = df['fonction'].fillna('')
    fcts = fcts[fcts.id_site == code_site]
    fcts = __fromatValue(fcts,'nom_fct')
    # fcts = __fromatValue(fcts,'description')
    # fcts.description = fcts.description
    fcts.description.replace(
        {**replace_spetialChar,**dictio},
        regex=True,inplace=True)
    Eval = df['evaluation'].fillna('').replace({'’':"'"},regex=True)
    Eval = Eval[Eval.id_site == code_site]
    Eval = __fromatValue(Eval,'rmq_interet_patri')

    Id = info.id.values[0]
    titre = info.nom.values[0]
    area  = (geom.area.values[0] / 10000).round(2)
    commune = info.nom_com.values[0]
    if subc.connexion.dropna().empty:
        typ_con = None
    else: 
        typ_con = subc.connexion.dropna().unique()[0]

    last_maj = habs.date_habit.max()
    if 'auteur_hab' in habs.columns:
        user_maj = habs[habs.date_habit==last_maj].auteur_hab
    else:
        user_maj = habs[habs.date_habit==last_maj].nom_prenom
    if isinstance(delm.date_critd.max(),datetime.date):
        if last_maj < delm.date_critd.max():
            last_maj = delm.date_critd.max()
            user_maj = delm[delm.date_critd==last_maj].auteur_delim
    if isinstance(desc.date_usage.max(),datetime.date):
        if last_maj < desc.date_usage.max():
            last_maj = desc.date_usage.max()
            user_maj = desc[desc.date_usage==last_maj].auteur_usage
    if isinstance(o_in.date_reghy.max(),datetime.date):
        if last_maj < o_in.date_reghy.max():
            last_maj = o_in.date_reghy.max()
            user_maj = o_in[o_in.date_reghy==last_maj].auteur_hydro
    if isinstance(o_ou.date_reghy.max(),datetime.date):
        if last_maj < o_ou.date_reghy.max():
            last_maj = o_ou.date_reghy.max()
            user_maj = o_ou[o_ou.date_reghy==last_maj].auteur_hydro
    if 'date_sub' in subc.columns and isinstance(subc.date_sub.dropna().max(),datetime.date):
        if last_maj < subc.date_sub.dropna().max():
            last_maj = subc.date_sub.dropna().max()
            user_maj = subc[subc.date_sub==last_maj].auteur_sub
    if isinstance(subc.date_conne.dropna().max(),datetime.date):
        if last_maj < subc.date_conne.dropna().max():
            last_maj = subc.date_conne.dropna().max()
            user_maj = subc[subc.date_conne==last_maj].auteur_connect
    if isinstance(fcts.date_fctec.max(),datetime.date):
        if last_maj < fcts.date_fctec.max():
            last_maj = fcts.date_fctec.max()
            user_maj = fcts[fcts.date_fctec==last_maj].auteur_fct
    auth_maj = {'usr':user_maj.values[0],'dat':last_maj}

    # pdf = FPDF(format='A4') #pdf format
    # pdf = PDF(code=Id)
    pdf = PDF()
    pdf.code = Id
    pdf.set_title(titre)
    pdf.add_page() #create new page
    # Titre(pdf,titre)
    FisrtLign(pdf,Id,area,commune)
    top = pdf.y             # Save top coordinate
    offset = pdf.x + 87    # Calculate x position of next cell
    __separateLine(pdf)
    set_y = Infos_zh(pdf,info,delm,auth_maj)

    i = image_parsing.get_img_info(img1)
    W = i['w']/6
    H = i['h']/6
    pdf.image(img1,x=offset,y=top,w=W,h=H)
    pdf.set_col(1.5)    # set column
    # pdf.set_y(pdf.y-23) # Reset y coordinate
    # pdf.set_y(set_y) # Reset y coordinate
    # sst1 = 'Remarque'
    # para(pdf,sst1,u'%s'%Eval.rmq_interet_patri.values[0], heigth=5, width=95, size=10)
    pdf.set_col(0)      # set column
    # __separateLine(pdf)


    Fcmt_zh(pdf,o_in,o_ou,typ_con)
    pdf.set_col(0)      # set column
    # __separateLine(pdf)
    Habs_zh(pdf,habs,img2,offset)

    if pdf.page_no() == 1:
        pdf.add_page()      # create new page
    # pdf.ln()

    if fileprio is not None and rvb_priorite is not None and rvb_objectif is not None:
        render_table(fileprio,code_site,rvb_priorite,rvb_objectif)


    UsgProcess_zh(pdf, desc)
    
    sst1 = 'Valeurs socio-économiques'
    val_socioEco = fcts[fcts.type=='val_socioEco']
    Fcts_zh(pdf,val_socioEco,sst1)
    sst1 = 'Fonctions hydrologiques'
    fct_hyd = fcts[fcts.type=='fct_hydro']
    Fcts_zh(pdf,fct_hyd,sst1)
    sst1 = 'Fonctions biologiques'
    fct_bio = fcts[fcts.type=='fct_bio']
    Fcts_zh(pdf,fct_bio,sst1)
    sst1 = 'Intérêts patrimoniaux'
    int_patri = fcts[fcts.type=='int_patri']
    Fcts_zh(pdf,type_fct=int_patri,sst=sst1)

    pdf.ln(10)
    sst1 = 'Remarque(s)'
    para(pdf,sst1,
        u'%s'%Eval.rmq_interet_patri.values[0], 
        heigth=5, 
        width=190, 
        size=10
    )

    pdf.output(fout)




sql = """
    SELECT * FROM zones_humides.v_zoneshumides WHERE site_code in (%s)
"""%str(lst_codesite).replace(r'[','').replace(']','')
VZH = gpd.read_postgis(sql,zh.con)
DF  = zh.get_bilan(code_site=lst_codesite)
# GDF = zh.get_sitesGeom(id_site=lst_codesite)
map1 = ctx.providers.OpenTopoMap
# map2 = ctx.providers.GeoportailFrance.orthos
# map2['url'] = "https://data.geopf.fr/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER={variant}&STYLE={style}&FORMAT={format}&TILEMATRIXSET=PM_0_19&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}"
# map2['variant'] = "ORTHOIMAGERY.ORTHOPHOTOS"

# map2 = ctx.tile.TileProvider(
#     name="GoogleSatellite",
#     url="https://data.geopf.fr/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=ORTHOIMAGERY.ORTHOPHOTOS.ORTHO-EXPRESS.2024&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM_0_19&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
#     attribution="OSM contributors, GoogleSatellite",
#     max_zoom=21,
# )
map2 = ctx.tile.TileProvider(
    name="GoogleSatellite",
    url="https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}",
    attribution="OSM contributors, GoogleSatellite",
    max_zoom=18,
)
# map2 = ctx.providers.Esri.WorldImagery
# map2 = ctx.providers.CartoDB.Voyager
# map2 = ctx.providers.Esri.WorldTerrain
# map2 = ctx.providers.Esri.WorldShadedRelief
# map1 = ctx.providers.Esri.WorldStreetMap
# map1 = ctx.providers.Esri.WorldTopoMap
# map1 = ctx.providers.MtbMap
# zoom = 'auto'
PATH_PRIO = "/media/colas/SRV/FICHIERS/STRUCTURES ET PROCEDURES/PROCEDURES/CR SUD GRESIVAUDAN/PGSZH Sud Grésivaudan/Rapport/tableaux/Résultats analyses SIG/"
fileprio = PATH_PRIO+"Résultats_etude_PGSZH_SudGresivaudan_VF.xlsx"
rvb_priorite = gpd.pd.read_csv(PATH_PRIO+'rvb_priorite.csv',sep=";")
rvb_objectif = gpd.pd.read_csv(PATH_PRIO+'rvb_objectif.csv',sep=";")

dict_cols_readEnjeux = {
    'Code ZH':'id_site',
}
dcols_pdfEnjeux = {
    'Pressions directes':'Pression sur la ZH',
    'Pressions indirectes':'Pression autour de la ZH',
}

for code_site in lst_codesite[:] :  # 14 OK
    # code_site = lst_codesite[14]
    # lst_codesite = [code_site]
    # code_site = '38RD0095'
    # df   = zh.get_bilan(code_site=code_site)
    # geom = zh.get_sitesGeom(id_site=code_site)
    fout = path0 + 'RAPPORT_ZH/PGSZH/%s.pdf' % code_site
    # geom = GDF[GDF.id_site==code_site]
    geom = VZH[VZH.site_code==code_site]

    create_maps(geom, map1, map2, zoom='auto')  # maps2 : ['auto', 19,18,17,16,15]
    # create_pdf(df=DF, code_site=code_site,fileprio=fileprio,rvb_priorite=rvb_priorite,rvb_objectif=rvb_objectif)
    create_pdf(df=DF, code_site=code_site)

    nb = '%s/%s' % (lst_codesite.index(code_site)+1,len(lst_codesite))
    print(" %s  %s OK ..." % (nb,code_site) )

remove(img1)
remove(img2)

