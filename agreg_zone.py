from os.path import join
from os import listdir
import geopandas as gpd

SRV = '/media/colas/SRV/FICHIERS'
PATH = 'OUTILS/CARTOGRAPHIE/ESPACE DE TRAVAIL/ETUDES/PGSZH_Belledonne/Pressions/PLU'

DICT_COLS = {
    'TYPESECT':'TYPEZONE'
}
KEEP_ZONE = ['U','AU','AUs','AUc']

DF = gpd.GeoDataFrame()
for f in listdir(join(SRV,PATH)):

    if f[-3:] != 'shp' : pass
    else:
        df = gpd.read_file(join(SRV,PATH,f))
        df.columns = df.columns.str.upper()
        df.rename(columns=DICT_COLS, inplace=True)
        DF['SOURCE'] = f
        print(df.TYPEZONE.unique())
        tmp = df[df.TYPEZONE.isin(KEEP_ZONE)]
        DF = gpd.pd.concat([DF,tmp])

DF.set_geometry('GEOMETRY',inplace=True)
DF.rename_geometry('geom', inplace=True)
DF.set_crs(2154,inplace=True)

DF.to_file(join(SRV,PATH,'COUCHES_AGREGEES','FILTREZONE_URBA_U&AU.gpkg'),options={'OVERWRITE=YES'})
DF.to_file(join('~/Documents/tmp','FILTREZONE_URBA_U&AU.gpkg'))