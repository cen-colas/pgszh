# PGSZH
Script de calcul d'indices caratérisants les zones humides d'un territoire dans le cadre d'un plan de gestion stratégique.

## Install
pip install git+https://framagit.org/cen-colas/pycen.git

  ## Critères concidérés

- Fonctions :
  - Fonctions biologiques et écologiques :
    - **znieff_1 :** Calcul la présence/absence des zones humides sur des ZNIEFF1 par intersection. (Présence 2 / Absence 0)
    - **redi :** Calcul la capacité de déplacement de la faune du réseau écologique départemental de l'Isère (REDI) par intersection. Utilisation d'un buffer de 100m (Présence 2 / Absence 0)
    - **fct_bio :** Dans le cas où un zone humide n'intersecte pas une ZNIEFF1, attribution d'un poids à chaque zone humide pour ses fonctions Biologiques et Ecologiques décritent dans les inventaires "zones humides".
    (Fonction 1 / Multi-fonctions 2)
    - **fct_hab :** Attribution d'un poids à chaque zone humide en fonction du type d'habitat décrit dans les inventaires "zones humides". ('prioritaire|très rare' 2)
    - **fct_esppatri :** Attribution d'un poids à chaque zone humide en fonction des espèces protégées (DH|PR|DO|LR|PN|LRR|LRN|LRM|LRF|LRD|LRE) décritent dans les inventaires "zones humides". (0 < nb esp < 3 : 0.5 / nb esp >= 3 : 1)


  - Fonctions hydrologiques :
    - **zone_inond :** Calcul la présence/absence des zones humides sur des zones inondables par intersection (Présence 1 / Absence 0).
    - **eabf :** Calcul de l'espace alluvial de bon fonctionnement (EABF)
    ou de fond de vallée par intersection (Présence 1 / Absence 0).
    - **dist_reso_hydro :** Si la zone humide ne possède pas d'espace
    alluviale de bon fonctionnement d'après la fonction "eabf()", calcul la distance au réseau hydrographique linéaire (le plus proche). Attribution d'un poids en fonction de la distance. Si la zone ne possède pas d'eabf et ne semble pas à proximité d'un réseau hydrique, recherche de la présence d'un cours d'eau dans la base de données zones humes. (_> 50m_ : 0 / _]10 m – 50 m]_ : 0.5 / _<= 10m_ : 1)
    - **reghydro_out :** Pour chaque zone humide, en cas de distance
    au réseau hydrographique linéaire > 50 et d'absence d'espace alluviale de bon fonctionnement, recherche dans la base de données des zones humides si une sortie d'eau "Cours d'eau"est définie. Attribution d'un poids en fonction (Si "Cours d'eau" : 1).
    - **connex_molasse :** Attribution d'un poids à chaque zone humide en fonction de sa connexion avérée à la molasse ou non (Présence 1 / Absence 0).
    - **idpr :** Calcul de l'Indice de Développement et de Persistance des Réseaux. Calcul réalisé dans le cas où connex_molasse = 0 (Si recouvrement > 25% : 1).
    - **fct_hydro :** Attribution d'un poids à chaque zone humide en fonction du nombre de rôles hydro-biologiques à caractères hydrauliques et hydrologiques qu'elle remplie (Fonction 0.5 / Multi-fonctions 1).
    - **zse_zsnea :** Attribution d'un poids à chaque zone humide en fonction de sont appartenance à une zone de sauvegarde exploitée actuelle (zse) ou future (zsnea) (Présence 1 / Absence 0).

  - Fonctions physiques et biochimiques :
    - **perim_captage :** Identification de la présence/absence de zones de captages à proximité des zones humides par intersection (Si intersection : 2).
    - **fct_hydrobio :** Attribution d'un poids à chaque zone humide en fonction du nombre de rôles hydro-biologiques à caractères physiques et biochimiques qu'elle remplie (Fonction 1 / Multi-fonctions 2).
    - **occup_sol :** Pour chaque zone humide, identification de la nature d'occupation du sol et de sa surface de recouvrement global (). Déduction de la surface d'espace naturel concernée par les zonnages. Attribution d'un poids en fonction de la surface de recouverte (_[ 25% ; 50% [_ : 1 / _[ 50% ; 75% [_ : 1.5 / _[ 75% ; 100% ]_ : 2).

  - Criètes « multi-fonctions » :
    - **surface :** Calcul de la surface totale des zones humides. Attribution d'un poid en fonction du résultat ( _[ 1ha ; 20ha [_ : 0.5 /  _[ 20ha ; 100ha [_ : 1 / _>= 100ha_ : 1.5).
    - **pente :** Calcul de la pente moyenne des zones humides via le MNT. Attribution d'un poid en fonction du résultat (moy(pente) < 5% : 1).
    - **dir_exp :** Ajout d'un champ dir_exp dans le tableau de sortie qui sera à remplir manuellement par celui-ci.

- Pressions :
  - Directes :
    - **artif_directe :** Récupération des résultats des pressions directes d'artificialisation Rhoméo I12. Application de la discrimination de Jenks pour catégoriser les résultats en 3 classes [0, 0.5, 1].
    - **urbani_directe :** Récupération des résultats des pressions directes d'urbanisation Rhoméo I12. Application de la discrimination de Jenks pour catégoriser les résultats en 4 classes [0, 0.5, 1, 1.5].
    - **pressAgri_directe :** Récupération des résultats des pressions directes agricoles Rhoméo I13. Application de la discrimination de Jenks pour catégoriser les résultats en 3 classes [0, 0.5, 1].
    - **projet_plu_U :** Intersections des zones relevant du projet d'Urbanisme (PLU) avec les polygones de l'étude. Considération du champs Typezone == 'U'. Attribution des points en cas d'intersections (Présence 1 / Absence 0).
    - **conflit_redi :** Intersections des zones de conflits redi (Points, Lignes) avec les polygones de l'étude. Utilistaion d'un buffer de 100m (Présence 2 / Absence 0).
    - **prelev_eau :** Identification da la proximité des zones humides avec des sources de captages. Application d'un buffer de 50m. Identification par intersection (Présence 1 / Absence 0).
    - **icpe :** Identification da la proximité des zones humides avec des installations classés.Application d'un buffer de 500m. Identification par intersection (Présence 1 / Absence 0).
    - **ouvrage :** Identification da la présence d'ouvrages et de dépôts au sein des zones humides. Identification par intersection (Présence 1 / Absence 0).
    - **vulnerabilite :** Identification da la proximité des zones humides avec des espèces exotiques envahissantes. Application d'un buffer de 100m. Identification par intersection (Présence 1 / Absence 0).
  - Indirectes :
    - **artif_indir :** Récupération des résultats des pressions indirectes d'artificialisation Rhoméo I12. Application de la discrimination de Jenks pour catégoriser les résultats en 3 classes [0, 0.5, 1].
    - **urbani_indir :** Récupération des résultats des pressions indirectes d'urbanisation Rhoméo I12. Application de la discrimination de Jenks pour catégoriser les résultats en 4 classes [0, 0.5, 1, 1.5].
    - **pressAgri_indir :** Récupération des résultats des pressions indirectes agricoles Rhoméo I13. Application de la discrimination de Jenks pour catégoriser les résultats en 3 classes [0, 0.5, 1].
    - **projet_plu_AU :** Intersections des zones relevant du projet d'Urbanisme (PLU) avec les polygones de l'étude. Considération du champs Typezone == 'AU'. Attribution des points en cas d'intersections (Présence 1 / Absence 0).
    - **projet_scot :** En cas d'absence de PLU, recherche d'espaces de développements potentiels au alentours des sites (SCOT). Attribution des points en cas d'intersections (Présence 1 / Absence 0).